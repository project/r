<?php

namespace Drupal\r\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a settings form for R Filter module.
 */
class RSettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'r_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => t('R binary'),
      '#default_value' => \Drupal::state()->get('r.path', '/usr/bin/R'),
      '#size' => 50,
      '#maxlength' => 255,
      '#description' => t('Absolute path to the R binary.', [
        ':bin' => '/usr/bin/R',
        ':local' => '/opt/homebrew/bin/R',
      ]),
    ];

    $form['tmp'] = [
      '#type' => 'textfield',
      '#title' => t('Temporary files path'),
      '#default_value' => \Drupal::state()->get('r.tmp', '/tmp'),
      '#size' => '50',
      '#maxlength' => '255',
      '#description' => t('Absolute path to Temporary directory.'),
    ];

    $form['mode'] = [
      '#type' => 'checkbox',
      '#title' => t('Minimal R mode'),
      '#default_value' => \Drupal::state()->get('r.mode', 0),
      '#description' => t('Run R in silent mode with minimal output.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $file_exists = file_exists($form_state->getValue('path'));
    if (!$file_exists) {
      $form_state->setErrorByName('path', t('R binary  not found.'));
    }

    $is_dir = is_dir($form_state->getValue('tmp'));
    if (!$is_dir) {
      $form_state->setErrorByName('tmp', t('TMP directory not found.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    \Drupal::state()->set('r.path', $form_state->getValue('path'));
    \Drupal::state()->set('r.tmp', $form_state->getValue('tmp'));
    \Drupal::state()->set('r.mode', $form_state->getValue('mode'));

    \Drupal::messenger()->addMessage(t('R settings form saved.'));
  }

}
