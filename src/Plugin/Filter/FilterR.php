<?php

namespace Drupal\r\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Embed R output in Drupal content types.
 *
 * @Filter(
 *   id = "filter_r",
 *   title = @Translation("R Filter"),
 *   description = @Translation("Embed R output in Drupal content types."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   weight = 0,
 * )
 */
class FilterR extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {

    return new FilterProcessResult(preg_replace_callback('/\[R\](.*?)\[\/R\]/s', [&$this, 'rFilterCallback'], $text));

  }

  /**
   * The callback for preg_relpace_callback above. Replaces code with output.
   */
  public function rFilterCallback($matches) {

    $r_path = \Drupal::state()->get('r.path', '/usr/bin/R');
    $tmp_path = \Drupal::state()->get('r.tmp', '/tmp');
    $mode = (int) \Drupal::state()->get('r.mode', 0);

    $hash = md5($matches[1]);

    if (file_exists($tmp_path . '/R.' . $hash . '.out')) {
      // Command was already executed. Don't run again. Get the cached output.
      return '<span class="r">' . file_get_contents($tmp_path . '/R.' . $hash . '.out') . '</span>';
    }
    else {
      // New R command. Execute and return output.
      $fh = fopen($tmp_path . '/R.' . $hash . '.in', 'w+');
      fwrite($fh, $matches[1]);
      fclose($fh);

      $mode = $mode ? '--no-echo' : '--quiet';
      shell_exec('cd ' . $tmp_path . ';' . $r_path . ' CMD BATCH ' . $mode . ' --no-timing ' . $tmp_path . '/R.' . $hash . '.in ' . $tmp_path . '/R.' . $hash . '.out');
      return '<span class="r">' . file_get_contents($tmp_path . '/R.' . $hash . '.out') . '</span>';

    }
  }

}
