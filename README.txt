R is a free software environment for statistical computing and graphics.

The R Filter module is used to create an interface to R within a node. This interface is created by means of a Drupal filter which may be enabled for a particular text format. The R Filter module is intended to be useful for presenting output from R commands on Drupal content types.

Place R code between R square brackets [R][/R] in the body of node. Assuming a text format with R Filter enabled was selected, the code will have been executed and the output displayed inline within the the text body. Place supporting text outside of the R square brackets.

GRANT FILTER ACCESS WITH CARE. It is NOT recommended to give anonymous users access to R filter. 

R output is cached until the caches are cleared.

LIMITATIONS

The R Filter module will not work with CKEditor enabled for the text format. CKEditor replaces '<' and '>' with '&lt;' and '&gt;', respectively. It also adds paragraph and line break tags which causes errors in R.

PREREQUISITES

1. Make sure R and Drupal are installed on the target webserver.

INSTALLATION

1. Download and enable the R Filter module.

2. Configure the module at /admin/config/content/r.

3. Enable the R Filter for the text format that will execute the R code.

4. Disable CKEditor from the dropdown. You may want to create a special text format just for R.

Once installed, start creating content like this. You may need to adjust the commands to suit your environment.

[R]
rnorm(50, 0, 1)
[/R]

[R]
png('/var/www/html/web/sites/default/files/img/reg.png')
x <- rnorm(100);
y <- exp(x) + rnorm(100);
result <- lsfit(x,y);
ls.print(result);
plot(x, y);
abline(result);
lines(lowess(x,y), col=2);
dev.off();
png('/var/www/html/web/sites/default/files/img/res.png');
hist(result$residuals);
dev.off();
[/R]

<img src="/sites/default/files/img/reg.png" alt="R statistical graph">
<img src="/sites/default/files/img/res.png" alt="R residuals graph">

To prevent the disclosure of the absolute path of an image create a .Rprofile file and place it in your home directory or the directory where the R filter will be executed:

img_dir <- '/var/www/html/web/sites/default/files/img/';

And when creating the image inside the Drupal content type replace the png command seen above with:

[R]
png(paste(img_dir,'plot.png',sep=''));
x <- rnorm(50, 0, 1)
y <- rnorm(50, 0, 1)
plot(x,y)
dev.off();
[/R]

<img src="/sites/default/files/img/plot.png" alt="This is my R plot">

CREDITS

Originally created by:

pmagunia - https://www.drupal.org/user/763420

Current Maintainers:

pmagunia - https://www.drupal.org/user/763420
