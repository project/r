<?php

namespace Drupal\Tests\r\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\filter\Entity\FilterFormat;

/**
 * Functional tests for R Filter module.
 *
 * @group r
 */
class RUiPageTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'filter',
    'node',
    'r',
  ];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with administration permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);

    $full_html_format = FilterFormat::create([
      'format' => 'full_html',
      'name' => 'Full HTML',
      'weight' => 1,
      'filters' => [
        'filter_r' => [
          'status' => TRUE,
          'weight' => -10,
        ],
      ],
    ]);
    $full_html_format->save();

    $this->adminUser = $this->drupalCreateUser([
      'administer filters',
      'administer site configuration',
    ]);

    $this->drupalLogin($this->adminUser);
  }

  /**
   * Make sure R Filter checkbox is enabled.
   */
  public function testFilter() {
    $this->drupalGet('admin/config/content/formats/manage/full_html');
    $web_assert = $this->assertSession();
    $web_assert->statusCodeEquals(200);
    $web_assert->pageTextContains("Full HTML");
    $web_assert->pageTextContains("R Filter");
  }

  /**
   * Make sure the Settings page loads correctly.
   */
  public function testSettingsPage() {
    $this->drupalGet('admin/config/content/r');
    $web_assert = $this->assertSession();
    $web_assert->statusCodeEquals(200);
    $web_assert->pageTextContains("R Settings");
    $web_assert->pageTextContains("R binary");
  }

}
